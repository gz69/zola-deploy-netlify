+++
title = "Curriculum Vitae"
date = 2024-01-26T15:00:00Z

+++

<h1 align = "center">🎓 Education Background 🎓</h1>
------

[`Duke University`](https://duke.edu/), `Durham, United States 🇺🇸`

Master of Engineering in `Electrical and Computer Engineering` (Software Development Track), `08/2022-05/2024`

- GPA: `3.83/4`; Scholarship recipient (20% of tuition fee) for the first academic year
- Relevant courses: Fundamentals of Computer Systems and Engineering (A), Systems Programming and Engineering (A), Software Engineering (A), Programming, Data Structures, and Algorithms in C++ (A)
<br/>

[`LUISS University`](https://www.luiss.edu/), `Rome, Italy 🇮🇹`

Bachelor of Science in `Management and Computer Science`, `09/2018-11/2021`

- GPA: `109/110 (3.77/4)`; Full scholarship recipient (Including accommodation fee) in 3 consecutive years
- Relevant courses: Algorithms (A+), Databases & Big Data (A+), Microeconomics (A+), Macroeconomics (A+), Data Analysis for Business (A+), Statistics (A+), Mathematical Finance (A+), Artificial Intelligence and Machine Learning (A), Fundamentals of Management (A), Quantitative Models for Data Science (A), Introduction to Computer Programming (A)

<h1 align = "center">👨🏻‍💻 Experience 👨🏻‍💻</h1>
------

[`Mevion Medical Systems`](https://www.mevion.com/)

`Software Engineer intern` `[C++ & JIRA]`, `04/2022-07/2022`

- Developed and enhanced the Room 3D View feature at Mevion, addressing display issues, improving user experience, and ensuring safe movement of the treatment couch.
- Researched collision detection techniques, collaborated cross-functionally, and implemented effective solutions to enhance the user interface and facilitate seamless navigation.
<br/>

[`Bank of Beijing Fintech`](https://bankofbeijing.com.cn/en)

`Artificial Intelligence Developer intern` `[Python & Docker]`, `04/2022-07/2022`

- Collected user text data with intelligence robot assistant from application API. Constructed various Natural language
processing (NLP) algorithm models, parameters tuning and uploaded them to the Docker warehouse to help the project.
- Scrapped hundreds of pages of data from Beijing Municipal Health Commission with a Scrapy framework into database.
<br/>

[`Kantar TNS`](https://www.kantar.com/)

`Data analytics intern` `[Python & Excel]` `06/2020-09/2020`
- Consulted for consumer and retail companies, providing professional data analytics services with Python toolkits. Identified optimal product portfolios and market shares, improving business performance by 87% and program efficiency by 30%.
- Constructed comprehensive data visualization reports for clients, demonstrating strong leadership and involvement in successful project delivery. Resulted in high levels of satisfaction and positive feedback from clients.

<h1 align = "center">📌 Additional Information 📌</h1>
------

`Languages:`

`🇨🇳 Chinese Mandarin (Native)`    `🇺🇸 English (Proficient)`    `🇮🇹 Italian (Intermediate)`

`Skills:` Mastered in `ChatGPT`, `C/C++`, `Python`, `Java`, `Software R`, `SQL database`, `UML Diagram`, `Markdown`, `Stata`, `LaTeX`

`Certificates:` `TOEFL: 111(23)`, `European Computer Driving License (ECDL/ICDL)`, `Google Analytics
Certification`, `Programming Fundamentals by Duke University (Coursera)`, `Introduction to Data Science in Python by UMich (Coursera)`, `Entrepreneurship by UPenn (Coursera)`


