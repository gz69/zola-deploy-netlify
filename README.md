# Deployment Guide

This document provides the steps for deploying this project using GitLab CI/CD and Netlify.

## Deploying with GitLab CI/CD

This project is configured to automatically deploy the website to GitLab Pages using a `.gitlab-ci.yml` file. Here's how to set it up:

### `.gitlab-ci.yml` Configuration

Create a file named `.gitlab-ci.yml` in the root directory of your project and paste the following configuration:

```yaml
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  GIT_SUBMODULE_STRATEGY: "recursive"
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
    - |
      if [ -n "$ZOLA_VERSION" ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found."
          exit 1
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(wget --output-document - $github_api_url | grep "browser_download_url.*linux-gnu.tar.gz" | cut --delimiter : --fields 2,3 | tr --delete "\" ")
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

This configuration will deploy your site automatically when you push to the default branch (e.g., master or main).

## Deploying with Netlify

To deploy your site with Netlify, follow these steps:

1. Visit [Netlify](https://netlify.com) and sign in with your Git repository provider (GitHub, GitLab, Bitbucket).
2. Click on **New site from Git**.
3. Choose your Git provider and connect your repository.
4. In the **Build options, and deploy!** section, set the build command to `zola build` and the publish directory to `public`.
5. Click on **Deploy site** to complete the deployment.

Netlify will automatically rebuild and deploy your site every time you push to your repository.